export class Painting {
  id: string = '';
  title: string = '';
  description: string = '';
  size: string = '';
  previewImage: string = '';
  fullImage: string = '';
  type: string = '';
  isOpened: boolean = false;
  imageId: string = '';
  imageName: string = '';
}
