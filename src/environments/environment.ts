// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'painter-d6c9c',
    appId: '1:421909281508:web:4b5820f825ff50c551caf1',
    storageBucket: 'painter-d6c9c.appspot.com',
    locationId: 'europe-central2',
    apiKey: 'AIzaSyDglx_mSyN9YEnNxjMMcJAwInU7ihXNR_I',
    authDomain: 'painter-d6c9c.firebaseapp.com',
    messagingSenderId: '421909281508',
    measurementId: 'G-YRS5Q92QJF',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
